from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):

    url= f'https://api.pexels.com/v1/search?query={city}%20{state}'

    headers={"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)

    photos = json.loads(r.content)

    photo = {
        "photo_url": photos["photos"][0]["url"]
    }

    return photo



def get_weather_data(city, state):
    url= f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},840&appid={OPEN_WEATHER_API_KEY}'
    headers = {"Authorization": OPEN_WEATHER_API_KEY}

    r = requests.get(url, headers=headers)

    parsedr = json.loads(r.content)
    print(url)
    area = {
        "latitude": parsedr[0]["lat"],
        "longitude": parsedr[0]["lon"]
    }

    x = area.get("latitude")
    y = area.get("longitude")

    url = f'https://api.openweathermap.org/data/2.5/weather?lat={x}&lon={y}&appid={OPEN_WEATHER_API_KEY}'
    headers = {"Authorization": OPEN_WEATHER_API_KEY}

    request = requests.get(url, headers=headers)

    parsedrequest = json.loads(request.content)

    weather = {
        "temperature": parsedrequest["main"]["temp"],
        "description": parsedrequest["weather"][0]["description"]
    }

    return weather
